﻿#pragma strict

public var player1: GameObject;
public var player2: GameObject;
var audioBGM: AudioSource;
public var fadeIn: boolean = true;

public var leaving: boolean = false;
var light1 : Light;
var light2 : Light;
var timming : float = 0.0f;
var maxVolume:float = 1.0f;

function Start () 
{
	 light1 = player1.GetComponentsInChildren(Light)[0];
	 light2 = player2.GetComponentsInChildren(Light)[0] ;
	 
	 audioBGM = GetComponent(AudioSource);
	 audioBGM.playOnAwake = false;
	 audioBGM.Play();
	 audioBGM.loop = true;
	 audioBGM.volume = 0.0f;
}

function Update ()
{
	if (audio.volume < maxVolume && fadeIn)
		audio.volume += Time.deltaTime;
	else if (audio.volume > 0.0f && !fadeIn)
		audio.volume -= Time.deltaTime;
		
	
	var distance: Vector3 = player1.transform.position - player2.transform.position;
	if (distance.magnitude < maxVolume)
	{
		leaving = true;
		fadeIn = false;
	}
	
	if (leaving)
	{
		PlayersEncountered();
	}
}

function PlayersEncountered()
{
	//Move to last scene?
	//Debug.Log("HEEEEEY");
	timming += Time.deltaTime*0.5f;
	
	light1.intensity = Mathf.Lerp(1.0f, 8.0f, timming);
	light2.intensity = Mathf.Lerp(1.0f, 8.0f, timming);
	
	light1.spotAngle = Mathf.LerpAngle(40.0f, 180.0f, timming);
	light2.spotAngle = Mathf.LerpAngle(40.0f, 180.0f, timming);
	
	if (light2.spotAngle >=  180.0f)
	{
		ReadyForNextLevel();	
	}
}

function ReadyForNextLevel()
{

//Load next Level

//Reset values here
}