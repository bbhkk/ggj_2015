﻿using UnityEngine;
using System.Collections;

public class light_pos_ctrl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float factor = 0.1f;
		float x = Input.GetAxis ("Horizontal") * factor;
		float y = Input.GetAxis ("Vertical") * factor;

		transform.Translate (x, 0, y);
	}
}
