#pragma strict
//import System.IO;
//import System.Convert;

public var tilePrefab: GameObject;
//public var numberOfTiles: int = 10;
//public var tilesPerRow: int = 4;
public var numberOfTiles: int = 100;
public var tilesPerRow: int = 10;
public var distanceBetweenTiles: float = 1.0;
public var values : int [,];


//print(Application.persistentDataPath);
public var asset: TextAsset; // Assign that variable through inspector
private var assetText : System.String;
//var sr : StreamReader = new System.IO.StreamReader("DriveDirs.txt");
public var textures : Texture [];

function Start () {

	assetText = asset.text;
	assetText = assetText.Replace("\n","").Replace("\r","");
	//var l = assetText.length;
	//for (var i=0; i < l; i++) {
	//print(assetText[i]);
	//}

	CreateTiles();
}
 
function CreateTiles()
{
 	var xOffset: float = 0.0;
    var zOffset: float = 0.0;
    var xGlobalOffset : float = (numberOfTiles / tilesPerRow -1 )* 0.5f;
    var zGlobalOffset : float = (tilesPerRow +1) * 0.5f;
    
    FillTiles();
    
    for(var tilesCreated: int = 0; tilesCreated < numberOfTiles; tilesCreated += 1)
    {
        xOffset += distanceBetweenTiles;
         
        if(tilesCreated % tilesPerRow == 0)
        {
            zOffset += distanceBetweenTiles;
            xOffset = 0;
        }
        
        var j:int = xOffset;
        var i:int = zOffset	-1;
        
       //Debug.Log("Position-" + xGlobalOffset + "-"+zGlobalOffset);
        //var mycube:GameObject;
        if (values[i,j] == 2)     
        {
        	Instantiate(tilePrefab, Vector3(transform.position.x - xOffset + xGlobalOffset, transform.position.y, transform.position.z + zOffset - zGlobalOffset), transform.rotation);
        }
        if (values[i,j] != 3)  
       	{
       		Instantiate(tilePrefab, Vector3(transform.position.x - xOffset + xGlobalOffset, transform.position.y - distanceBetweenTiles, transform.position.z + zOffset - zGlobalOffset), transform.rotation);
    		
    	}//else Debug.Log("print 3"+i+ "-"+j);
    	/*
    	var myMeshRenderer:MeshRenderer = mycube.GetComponent(MeshRenderer);
        	if (myMeshRenderer)
        	{
        		myMeshRenderer.enabled = false;
        		Debug.Log("inside");
        	}*/
    }
}


function FillTiles()
{
	var row : int = numberOfTiles / tilesPerRow;	
    
    values = new int [row,tilesPerRow];
			
	for (var i: int = 0; i < row; i++)
	{		
		for (var j: int = 0; j < tilesPerRow; j++)
		{
				// Use file value
				//print("current index");
				//print (i*tilesPerRow+j);
				//print("max_length");
				//print(assetText.Length);
				values[i,j] = System.Char.GetNumericValue(assetText[i*tilesPerRow+j]);
				//print ("values:");
				//print(assetText[i*tilesPerRow+j]);
			///	print (values[i,j]);
		}
	}
 
}



