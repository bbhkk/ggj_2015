﻿using UnityEngine;
using System.Collections;

public class intensity_ctrl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	float factor = 2.5f;
	float noise = 0.0f;
	float noise_freq = 0.1f;

	// Update is called once per frame
	void Update () {
		noise = Random.Range (-3f, 3f);
		GetComponent<Light>().intensity = Mathf.Sin (Time.realtimeSinceStartup) * factor + Mathf.Sin(noise_freq) * noise;
	}
}
