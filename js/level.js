
//Class Level
// It loads a level after you create an instance of it.
// First call preload
// Later create, just before you want to enter in it.

Level = function (bgColor,tilemapLvl,tilesetImg,tilesetKey,layerName,tilemapPath,tilesetPath)
{
	this.mbgColor 		= bgColor;//#797878
	this.mtilemapLvl 	= tilemapLvl;//mazelevel
	this.mtilesetImg	= tilesetImg;//MazeLevel1
	this.mtilesetKey	= tilesetKey;//tiles
	this.mlayerName		= layerName;//World1

	this.mtilemapPath	= tilemapPath;//'assets/level_01.json'
	this.mtilesetPath	= tilesetPath;//'assets/Maze_Walkers_Tiles2.png'
}

Level.prototype.preload = function() 
{
    game.load.tilemap(mtilemapLvl, mtilemapPath, null, Phaser.Tilemap.TILED_JSON);
	game.load.image(mtilesetKey, mtilesetPath);
}

Level.prototype.create = function () 
{
	game.stage.backgroundColor = mbgColor;

    //  The 'mazelevel' key here is the Loader key given in game.load.tilemap
    map = game.add.tilemap(mtilemapLvl);

    //  The first parameter is the tileset name, as specified in the Tiled map editor (and in the tilemap json file)
    //  The second parameter maps this name to the Phaser.Cache key 'tiles'
    map.addTilesetImage(mtilesetImg, mtilesetKey);
    
    //  Creates a layer from the World1 layer in the map data.
    //  A Layer is effectively like a Phaser.Sprite, so is added to the display list.
    layer = map.createLayer(mlayerName);

    //  This resizes the game world to match the layer dimensions
    layer.resizeWorld();
}
